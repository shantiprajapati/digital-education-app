﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Media;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DigitalEducation.Droid;
using DigitalEducation.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(MediaPlayer_Android))]
namespace DigitalEducation.Droid
{
   public class MediaPlayer_Android: Interface_MediaPlayer
    {
        int clicks = 0;
        MediaPlayer player;
        public MediaPlayer_Android()
        {

        }
        public bool Play_PauseAudio(string url)
        {
            if (clicks == 0)
            {
                this.player = new MediaPlayer();
                this.player.SetDataSource(url);
                this.player.SetAudioStreamType(Stream.Music);
                this.player.PrepareAsync();
                this.player.Prepared += (sender, args) =>
                {
                    this.player.Start();
                };
                clicks++;
            }
            else if (clicks % 2 != 0)
            {
                this.player.Pause();
                clicks++;

            }
            else
            {
                this.player.Start();
                clicks++;
            }


            return true;
        }

        public bool StopAudio(bool val)
        {
            this.player.Stop();
            clicks = 0;
            Toast.MakeText(Forms.Context, "player Stop", ToastLength.Short).Show();

            return true;
        }
    }
}