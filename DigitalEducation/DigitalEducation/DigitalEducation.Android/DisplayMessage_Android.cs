﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Gms.Ads;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using DigitalEducation.Droid;
using DigitalEducation.Interfaces;
using Xamarin.Forms;

[assembly: Dependency(typeof(DisplayMessage_Android))]
namespace DigitalEducation.Droid
{
   public class DisplayMessage_Android : Interface_Messages
    {
        InterstitialAd _Interestitial;
        public DisplayMessage_Android()
        {
            _Interestitial = new InterstitialAd(Android.App.Application.Context);
            _Interestitial.AdUnitId = "ca-app-pub-5286344997766130/6603068698";
            LoadAds();
        }
        private void LoadAds()
        {
            var requestbuilder = new AdRequest.Builder();
            _Interestitial.LoadAd(requestbuilder.Build());
        }

        public void DisplayToast(string msg)
        {
            Toast.MakeText(Android.App.Application.Context, "" + msg, ToastLength.Short).Show();
        }
        public void Close_App()
        {
            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
        }

        public void ShareApp()
        {
            Intent intentsend = new Intent();
            intentsend.SetAction(Intent.ActionSend);
            intentsend.PutExtra(Intent.ExtraText, "अब कक्षा आठ से दस तक तथा सभी प्रकार के प्रतियोगिता की ऑनलाइन तैयारी अब आपके मोबाइल पर हिंदी और अंग्रेजी दोनों भाषाओ में" + "https://play.google.com/store/apps/details?id=com.DigitalEdu.DigitalEducation");
            intentsend.SetType("text/plain");
            Forms.Context.StartActivity(intentsend);
        }
        public void ShowAds()
        {
            if (_Interestitial.IsLoaded)
                _Interestitial.Show();
            LoadAds();
        }

        public string CheckingUpdate()
        {
            var context = global::Android.App.Application.Context;
            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionCode.ToString();
            //var context = global::Android.App.Application.Context;

            //PackageManager manager = context.PackageManager;
            //PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            //return info.VersionName;
        }

        public void OpenPlayStore()
        {
            string appPackageName = Forms.Context.PackageName;
            try
            {
                var rintent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("market://details?id=" + appPackageName));
                rintent.AddFlags(ActivityFlags.NewTask);
                Forms.Context.StartActivity(rintent);
            }
            catch (ActivityNotFoundException)
            {
                var intent = new Intent(Intent.ActionView, Android.Net.Uri.Parse("https://play.google.com/store/apps/details?id=" + appPackageName));
                intent.AddFlags(ActivityFlags.NewTask);
                Forms.Context.StartActivity(intent);
            }
        }

        public void OpenPdf(string filePath)
        {

            var file = System.IO.Path.Combine(Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads).ToString(), filePath);
            var uri = Android.Net.Uri.Parse(file);

            Intent intent = new Intent(Intent.ActionView);
            intent.SetFlags(ActivityFlags.ClearTop);
            intent.SetDataAndType(uri, "application/pdf");
            try
            {
                Xamarin.Forms.Forms.Context.StartActivity(intent);

            }
            catch (ActivityNotFoundException e)
            {
                Toast.MakeText(Xamarin.Forms.Forms.Context, "No Application Available to View PDF", ToastLength.Short).Show();
            }

        }
    }
}