﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DigitalEducation.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class NewHomePageContents : ContentView
	{
		public NewHomePageContents ()
		{
			InitializeComponent ();
            btnEnglish.Clicked += BtnEnglish_Clicked;
            btnHindi.Clicked += BtnHindi_Clicked;
            btnbook.Clicked += Btnbook_Clicked;
        }
        private void Btnbook_Clicked(object sender, EventArgs e)
        {
            int langCode = 3;
            Navigation.PushModalAsync(new Home(langCode));
        }

        private void BtnHindi_Clicked(object sender, EventArgs e)
        {
            int langCode = 2;
            Navigation.PushModalAsync(new Home(langCode));
        }

        private void BtnEnglish_Clicked(object sender, EventArgs e)
        {
            int langCode = 1;
            Navigation.PushModalAsync(new Home(langCode));
        }
    }
}