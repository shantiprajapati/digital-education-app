﻿using DigitalEducation.Interfaces;
using DigitalEducation.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DigitalEducation.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Home : ContentPage
	{
        bool showAd = false;
        public Home(int langCode)
        {
            InitializeComponent();
            if (langCode == 0)
            {
                HomePageContents.Content = new NewHomePageContents();
                CheckNotification();
                CheckUpdate();
            }
            else
            {
                HomePageContents.Content = new SurveysView(langCode);
            }
            
            btnHome.GestureRecognizers.Add(new TapGestureRecognizer(OnHomeTap));
            btnShare.GestureRecognizers.Add(new TapGestureRecognizer(OnShareTap));
            btnFacebook.GestureRecognizers.Add(new TapGestureRecognizer(OnFacebookTap));
            btnContact.GestureRecognizers.Add(new TapGestureRecognizer(OnContactUsTap));
            btnTweeter.GestureRecognizers.Add(new TapGestureRecognizer(OnTwitterTap));


        }

        private async void CheckUpdate()
        {
            ViewModel_Update model = new ViewModel_Update()
            {
                ID = 0
            };
            try
            {
                using (var client = new HttpClient())
                {
                    string BaseUrl = string.Empty;
                    BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                    string postapi = "api/MyWebAPI/CheckUpdate";
                    BaseUrl = BaseUrl + postapi;
                    client.BaseAddress = new System.Uri(BaseUrl);
                    client.DefaultRequestHeaders.Clear();
                    string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                    try
                    {

                        var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                        if (responce.IsSuccessStatusCode)
                        {
                            var results = responce.Content.ReadAsStringAsync().Result;
                            var data = JsonConvert.DeserializeObject<List<ViewModel_Update>>(results);
                            if (data.Count != 0)
                            {
                                string versionCode = DependencyService.Get<Interface_Messages>().CheckingUpdate();
                                if (data[0].VerSionNumber != versionCode)
                                {
                                    var action = await DisplayActionSheet(null, null, null, data[0].Message, "Update", "Close");
                                    switch (action)
                                    {
                                        case "Update":
                                            DependencyService.Get<Interface_Messages>().OpenPlayStore();
                                            break;
                                        case "Close":
                                            DependencyService.Get<Interface_Messages>().DisplayToast("New Function not installed in your Apps. Please Update shortly...");
                                            break;


                                        default:
                                            break;
                                    }

                                }
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        string st = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

        }

        private async void CheckNotification()
        {
            ViewModel_Notification model = new ViewModel_Notification()
            {
                ID = 0
            };
            try
            {
                using (var client = new HttpClient())
                {
                    string BaseUrl = string.Empty;
                    BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                    string postapi = "api/MyWebAPI/NotificationList";
                    BaseUrl = BaseUrl + postapi;
                    client.BaseAddress = new System.Uri(BaseUrl);
                    client.DefaultRequestHeaders.Clear();
                    string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                    try
                    {

                        var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                        HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                        if (responce.IsSuccessStatusCode)
                        {
                            var results = responce.Content.ReadAsStringAsync().Result;
                            var data = JsonConvert.DeserializeObject<List<ViewModel_Notification>>(results);
                            if (data.Count != 0)
                            {
                                await DisplayAlert("Digital Education", data[0].Message, "Ok");
                            }

                        }

                    }
                    catch (Exception ex)
                    {
                        string st = ex.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                string st = ex.Message.ToString();
            }

        }

        protected override bool OnBackButtonPressed()
        {
            if (showAd)
            {
                Device.BeginInvokeOnMainThread(async () => {
                    var result = await this.DisplayAlert("Digital Education", "Do you want to exit?", "Yes", "No");
                    if (result)
                    {

                        if (Device.OS == TargetPlatform.Android)
                        {

                            DependencyService.Get<Interface_Messages>().Close_App();
                        }



                    }
                });
            }
            else
            {
                showAd = true;
                DependencyService.Get<Interface_Messages>().ShowAds();
            }

            return true;
        }
        private void OnFacebookTap(View arg1, object arg2)
        {
            Device.OpenUri(new Uri("https://www.facebook.com/profile.php?id=100030201622723"));

        }

        private void OnShareTap(View arg1, object arg2)
        {
            // DependencyService.Get<Interface_Messages>().DisplayToast("1");
            DependencyService.Get<Interface_Messages>().ShareApp();
        }

        private void OnContactUsTap(View arg1, object arg2)
        {
            // HomePageContents = new ContactUsView();
             Navigation.PushModalAsync(new ContactUsPage());
            //string _BooksUrl = "http://sanjay2019-001-site1.ctempurl.com/content_books/E3d55ouYDo" + ".pdf";
            //viewBooks.Source = _BooksUrl;
           // DependencyService.Get<Interface_Messages>().OpenPdf(_BooksUrl);
           // Device.OpenUri(new Uri("http://sanjay2019-001-site1.ctempurl.com/content_books/E3d55ouYDo.pdf"));
        }

        private void OnTwitterTap(View arg1, object arg2)
        {
            //
            Device.OpenUri(new Uri("https://twitter.com/SudhirK14082325"));
        }

        private void OnHomeTap(View arg1, object arg2)
        {
            Navigation.PushModalAsync(new Home(0));
        }
    }
}