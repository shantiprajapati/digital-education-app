﻿using Android.Text;
using DigitalEducation.Interfaces;
using DigitalEducation.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DigitalEducation.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class SurveysView : ContentView
	{
        public ObservableCollection<Model_Tutorials> Tutorial_Items = new ObservableCollection<Model_Tutorials>();
        static int TutorialID = 0;
        static int BookID = 0;
        bool BooksClick = false;
        static int SubjectID = 0;
        static int ChapterID = 0;
        static int LanguageID = 0;
        static string HeadingName = "";
        static string TutorialName = "", SubjectName = "";
        bool _IsAudio = false;
        bool _IsVideo = false;
        string _UniqueCode = "";
        public SurveysView (int lngCode)
		{
			InitializeComponent ();
            LanguageID = lngCode;
            if (lngCode == 3)
            {
                GetBookList();
            }
            else
            {
                GetTutorialList(lngCode);
            }

           
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            LoadingImage.Source = "http://sanjay2019-001-site1.ctempurl.com/Images/loadings.gif";
            // btnVideoPlay.Clicked += BtnVideoPlay_Clicked;
            SurveysListView.ItemSelected += SurveysListView_ItemSelected;
            btnBack.GestureRecognizers.Add(new TapGestureRecognizer(BackButtonEvent));
        }
        private async void GetBookList()
        {
           
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            Model_Tutorials model = new Model_Tutorials()
            {
                BookID = 0
            };
            using (var client = new HttpClient())
            {
                string BaseUrl = string.Empty;
                BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                string postapi = "api/MyWebAPI/Get_BookList";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new System.Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                try
                {
                    Tutorial_Items.Clear();
                    var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                    if (responce.IsSuccessStatusCode)
                    {
                        var results = responce.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<Model_Tutorials>>(results);
                        for (int i = 0; i < data.Count; i++)
                        {
                            if (data[i].IsActive == true)
                            {
                                Model_Tutorials itm = new Model_Tutorials()
                                {
                                    BookID = data[i].BookID,
                                    Name = data[i].Heading,
                                    Images = "heading.png",
                                    ImageVisible = true,
                                    TextSizes = 20,
                                    TextColors = "Green",
                                    FontAttributes = "Bold",
                                    SlNo = i + 1,
                                    IsAudio = false,
                                    IsVideo = false,
                                    UniqueCode = data[i].UniqueCode,
                                    BackGroundColor= "LightBlue"
                                };
                                BooksClick = true;
                                Tutorial_Items.Add(itm);
                            }

                        }
                        SurveysListView.ItemsSource = Tutorial_Items;
                        loaders.IsVisible = false;
                        loadingCompleted.IsVisible = true;
                    }

                }
                catch (Exception ex)
                {
                    string st = ex.Message.ToString();
                }
            }
        }

        
        private void BackButtonEvent(View arg1, object arg2)
        {
            if (BookID != 0)
            {
                GetBookList();
                BookID = 0;
            }
            else
            {
                if (ChapterID != 0)
                {
                    GetChapterList(SubjectID);
                    txtHeading.Text = SubjectName;
                    ChapterID = 0;
                    DependencyService.Get<Interface_Messages>().ShowAds();
                }
                else
                {
                    if (SubjectID != 0)
                    {
                        GetSubjectList(TutorialID);
                        txtHeading.Text = TutorialName;
                        SubjectID = 0;
                    }
                    else
                    {
                        if (TutorialID != 0)
                        {
                            GetTutorialList(LanguageID);
                            TutorialID = 0;
                        }
                        else
                        {
                            DependencyService.Get<Interface_Messages>().ShowAds();
                            Navigation.PushModalAsync(new Home(0));
                        }
                    }
                }
            }

        }

        private void SurveysListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (BooksClick)
            {
                loadingCompleted.IsVisible = false;
                //  pdfBooks.IsVisible = true;
                BookID = (e.SelectedItem as Model_Tutorials).BookID;
                string _UniqueCode = (e.SelectedItem as Model_Tutorials).UniqueCode;
                string BooksName = (e.SelectedItem as Model_Tutorials).Name;
                string _BooksUrl = "http://sanjay2019-001-site1.ctempurl.com/content_books/" + _UniqueCode + ".pdf";
               
                 Device.OpenUri(new Uri(_BooksUrl));
            }
            else
            {
                if (TutorialID == 0)
                {
                    TutorialID = (e.SelectedItem as Model_Tutorials).TutorialID;
                    GetSubjectList(TutorialID);
                    HeadingName = (e.SelectedItem as Model_Tutorials).Name;
                    TutorialName = HeadingName;
                    txtHeading.Text = HeadingName;
                }
                else
                {
                    if (SubjectID == 0)
                    {
                        SubjectID = (e.SelectedItem as Model_Tutorials).SubjectID;
                        if (SubjectID != 0)
                            GetChapterList(SubjectID);
                        HeadingName = (e.SelectedItem as Model_Tutorials).Name;
                        SubjectName = HeadingName;
                        txtHeading.Text = HeadingName;
                    }
                    else
                    {
                        ChapterID = (e.SelectedItem as Model_Tutorials).ChapterID;
                        if (ChapterID != 0)
                            GetContents(ChapterID);
                        HeadingName = (e.SelectedItem as Model_Tutorials).Name;
                        _UniqueCode = (e.SelectedItem as Model_Tutorials).UniqueCode;
                        txtHeading.Text = HeadingName;
                    }
                }
            }
            //DependencyService.Get<Interface_Messages>().DisplayToast(ChapterID.ToString());
        }

        private async void GetContents(int chapterID)
        {
          
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            Model_Tutorials model = new Model_Tutorials()
            {
                ChapterID = chapterID
            };
            using (var client = new HttpClient())
            {
                string BaseUrl = string.Empty;
                BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                string postapi = "api/MyWebAPI/Get_Contents";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new System.Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                try
                {
                    var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                    if (responce.IsSuccessStatusCode)
                    {

                        var results = responce.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<Model_Tutorials>>(results);
                        if (data.Count != 0)
                        {
                            Tutorial_Items.Clear();
                            for (int i = 0; i < data.Count; i++)
                            {
                                string cont = Html.FromHtml(data[i].Contents).ToString().Trim();
                                Model_Tutorials itm = new Model_Tutorials()
                                {
                                    Images = "",
                                    Name = cont,
                                    ChapterID = data[i].ChapterID,
                                    ImageVisible = false,
                                    TextSizes = 17,
                                    TextColors = "Black",
                                    FontAttributes = "None",
                                    SlNo = 0,
                                    IsVideo = data[i].IsVideo,
                                    IsAudio = data[i].IsAudio,
                                    UniqueCode = data[i].UniqueCode,
                                    BackGroundColor = "White",
                                    ContentWidth=320
                                };
                                Tutorial_Items.Add(itm);
                                _IsAudio = data[i].IsAudio;
                                _IsVideo = data[i].IsVideo;
                                _UniqueCode = data[i].UniqueCode;
                            }
                            SurveysListView.ItemsSource = Tutorial_Items;
                            loaders.IsVisible = false;
                            loadingCompleted.IsVisible = true;
                            
                        }
                        else
                        {
                            DependencyService.Get<Interface_Messages>().DisplayToast("Comming soon... Try Again..");
                        }

                    }

                }
                catch (Exception ex)
                {
                    string st = ex.Message.ToString();
                }
            }
        }

        private async void GetChapterList(int subjectID)
        {
            BookID = 0;
           
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            Model_Tutorials model = new Model_Tutorials()
            {
                SubjectID = subjectID
            };
            using (var client = new HttpClient())
            {
                string BaseUrl = string.Empty;
                BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                string postapi = "api/MyWebAPI/Get_ChapterList";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new System.Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                try
                {
                    var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                    if (responce.IsSuccessStatusCode)
                    {

                        var results = responce.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<Model_Tutorials>>(results);
                        if (data.Count != 0)
                        {
                            Tutorial_Items.Clear();
                            for (int i = 0; i < data.Count; i++)
                            {
                                if (data[i].IsActive == true)
                                {
                                    Model_Tutorials itm = new Model_Tutorials()
                                    {
                                        Name = data[i].Name,
                                        SubjectID = data[i].SubjectID,
                                        ChapterID = data[i].ChapterID,
                                        Images = "chapter.png",
                                        ImageVisible = true,
                                        TextSizes = 20,
                                        TextColors = "Black",
                                        FontAttributes = "Bold",
                                        SlNo = i + 1,
                                        IsAudio = false,
                                        IsVideo = false,
                                        UniqueCode = "",
                                        BackGroundColor = "LightBlue",
                                        ContentWidth = 200
                                    };
                                    Tutorial_Items.Add(itm);
                                }

                            }
                            SurveysListView.ItemsSource = Tutorial_Items;
                            loaders.IsVisible = false;
                            loadingCompleted.IsVisible = true;
                        }
                        else
                        {
                            DependencyService.Get<Interface_Messages>().DisplayToast("Comming soon... Try Again..");
                        }

                    }

                }
                catch (Exception ex)
                {
                    string st = ex.Message.ToString();
                }
            }
        }

        private async void GetSubjectList(int tutorialID)
        {
            
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            Model_Tutorials model = new Model_Tutorials()
            {
                TutorialID = tutorialID
            };
            using (var client = new HttpClient())
            {
                string BaseUrl = string.Empty;
                BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                string postapi = "api/MyWebAPI/Get_SubjectList";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new System.Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                try
                {
                    var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                    if (responce.IsSuccessStatusCode)
                    {

                        var results = responce.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<Model_Tutorials>>(results);
                        if (data.Count != 0)
                        {
                            Tutorial_Items.Clear();
                            for (int i = 0; i < data.Count; i++)
                            {
                                if (data[i].IsActive == true)
                                {
                                    Model_Tutorials itm = new Model_Tutorials()
                                    {
                                        TutorialID = data[i].TutorialID,
                                        Name = data[i].Name,
                                        SubjectID = data[i].SubjectID,
                                        Images = "subject.png",
                                        ImageVisible = true,
                                        TextSizes = 20,
                                        TextColors = "Blue",
                                        FontAttributes = "Bold",
                                        SlNo = i + 1,
                                        IsAudio = false,
                                        IsVideo = false,
                                        UniqueCode = "",
                                        BackGroundColor = "LightGray",
                                        ContentWidth = 200
                                    };
                                    Tutorial_Items.Add(itm);
                                }

                            }
                            SurveysListView.ItemsSource = Tutorial_Items;
                            loaders.IsVisible = false;
                            loadingCompleted.IsVisible = true;
                        }
                        else
                        {
                            DependencyService.Get<Interface_Messages>().DisplayToast("Comming soon... Try Again..");
                        }

                    }

                }
                catch (Exception ex)
                {
                    string st = ex.Message.ToString();
                }
            }
        }

        private async void GetTutorialList(int lngcode)
        {
           
            loaders.IsVisible = true;
            loadingCompleted.IsVisible = false;
            Model_Tutorials model = new Model_Tutorials()
            {
                LanguageID = lngcode
            };
            using (var client = new HttpClient())
            {
                string BaseUrl = string.Empty;
                BaseUrl = "http://sanjay2019-001-site1.ctempurl.com/";
                string postapi = "api/MyWebAPI/Get_HeadingList";
                BaseUrl = BaseUrl + postapi;
                client.BaseAddress = new System.Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                string test = JsonConvert.SerializeObject(model, Formatting.Indented);
                try
                {
                    Tutorial_Items.Clear();
                    var content = new StringContent(test, System.Text.Encoding.UTF8, "application/json");
                    HttpResponseMessage responce = await client.PostAsync(BaseUrl, content);
                    if (responce.IsSuccessStatusCode)
                    {
                        var results = responce.Content.ReadAsStringAsync().Result;
                        var data = JsonConvert.DeserializeObject<List<Model_Tutorials>>(results);
                        for (int i = 0; i < data.Count; i++)
                        {
                            if (data[i].IsActive == true)
                            {
                                Model_Tutorials itm = new Model_Tutorials()
                                {
                                    TutorialID = data[i].TutorialID,
                                    Name = data[i].Heading,
                                    Images = "heading.png",
                                    ImageVisible = true,
                                    TextSizes = 20,
                                    TextColors = "Green",
                                    FontAttributes = "Bold",
                                    SlNo = i + 1,
                                    IsAudio = false,
                                    IsVideo = false,
                                    UniqueCode = "",
                                    BackGroundColor="LightBlue",
                                    ContentWidth=200
                                };
                                Tutorial_Items.Add(itm);
                            }

                        }
                        SurveysListView.ItemsSource = Tutorial_Items;
                        loaders.IsVisible = false;
                        loadingCompleted.IsVisible = true;
                    }

                }
                catch (Exception ex)
                {
                    string st = ex.Message.ToString();
                }
            }
        }
    }
}