﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace DigitalEducation.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContactUsPage : ContentPage
	{
		public ContactUsPage ()
		{
			InitializeComponent ();
            openEmail.GestureRecognizers.Add(new TapGestureRecognizer(openEmails));
            openWebsite.GestureRecognizers.Add(new TapGestureRecognizer(openWeb));
        }
        private void openWeb(View arg1, object arg2)
        {
            // DependencyService.Get<Interface_Messages>().DisplayToast("1");
            Device.OpenUri(new Uri("http://sanjay2019-001-site1.ctempurl.com/"));
        }
        private void openEmails(View arg1, object arg2)
        {

            Device.OpenUri(new Uri("mailto:brightindia2019@gmail.com"));
        }
    }
}