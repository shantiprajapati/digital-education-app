﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalEducation.Models
{
    public class Model_Tutorials
    {
        public int BookID { get; set; }
        public int TutorialID { get; set; }
        public string Name { get; set; }
        public string Heading { get; set; }
        public int LanguageID { get; set; }
        public int SubjectID { get; set; }
        public int ChapterID { get; set; }
        public int ContentID { get; set; }
        public string Contents { get; set; }
        public string Images { get; set; }
        public bool IsActive { get; set; }
        public int SlNo { get; set; }
        public bool IsAudio { get; set; }
        public bool IsVideo { get; set; }
        public string UniqueCode { get; set; }
        public bool ImageVisible { get; set; }
        public int TextSizes { get; set; }
        public string TextColors { get; set; }
        public string FontAttributes { get; set; }
        public string BackGroundColor { get; set; }
        public int ContentWidth { get; set; }
    }
}
