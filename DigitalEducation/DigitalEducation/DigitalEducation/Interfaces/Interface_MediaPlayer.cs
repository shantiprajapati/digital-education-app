﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalEducation.Interfaces
{
    public interface Interface_MediaPlayer
    {
        bool Play_PauseAudio(string url);
        bool StopAudio(bool val);
    }
}
