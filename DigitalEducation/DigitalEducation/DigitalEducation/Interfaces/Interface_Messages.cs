﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalEducation.Interfaces
{
    public interface Interface_Messages
    {
        void DisplayToast(string msg);
        void Close_App();
        void ShareApp();
        void ShowAds();
        string CheckingUpdate();
        void OpenPlayStore();
        void OpenPdf(string url);
    }
}
