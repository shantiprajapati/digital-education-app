﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalEducation.ViewModels
{
    public class ViewModel_Update
    {
        public int ID { get; set; }
        public string Message { get; set; }
        public string VerSionNumber { get; set; }
    }
}
