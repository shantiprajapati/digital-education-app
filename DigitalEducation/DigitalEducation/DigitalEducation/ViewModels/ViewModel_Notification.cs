﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DigitalEducation.ViewModels
{
    public class ViewModel_Notification
    {
        public int ID { get; set; }
        public string Message { get; set; }
    }
}
